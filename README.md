iWand
========

iWand is a ROS based experimantal project to test drawing 3D objects in vertual space using tracking of real world object such as a white-board marker. Two of the initial componant of this project which are Simtrack and iai_kinect are used as is, with only cinfirgational changes. 

Test Video: [iWand](https://youtu.be/-OLFaq7mcT0)

Links and short descriptions to adopted projects:
-----------------------------------------------------------

[SimTrack](http://www.karlpauwels.com/simtrack), a simulation-based framework for tracking, is a [ROS](http://www.ros.org/)-package for detecting and tracking the pose of multiple (textured) rigid objects in real-time. SimTrack is released under the [BSD-license](http://opensource.org/licenses/BSD-3-Clause). SimTrack uses SiftGPU for feature extraction. Note that SiftGPU has a different [license](siftgpu/license.txt). Please cite the following paper if you use SimTrack in your research:

*Pauwels, Karl and Kragic, Danica (2015) [SimTrack: A Simulation-based Framework for Scalable Real-time Object Pose Detection and Tracking](http://www.karlpauwels.com/downloads/iros_2015/Pauwels_IROS_2015.pdf). IEEE/RSJ International Conference on Intelligent Robots and Systems, Hamburg, Germany, 2015.*

For more details see the following paper: 

*Pauwels, Karl; Rubio, Leonardo; Ros, Eduardo (2015) [Real-time Pose Detection and Tracking of Hundreds of Objects](http://www.karlpauwels.com/downloads/tcsvt_2015/Pauwels_IEEE_TCSVT_2015.pdf). IEEE Transactions on Circuits and Systems for Video Technology, in press.*

[IAI Kinect2](https://github.com/code-iai/iai_kinect2) is opensource driver package for Microsoft Kinect Sensor (V2) maanged by [Thiemo Wiedemeyer](https://github.com/code-iai/iai_kinect2)



Goal for iWand (Kinect Version)
-------------------------

Following are some of the main goals for developing and setting up the following project:

* Affordable by the makers community
* Robust in terms of adopting to tracking objects (multiple types of pen)
* Maximize tracking and avoid occlusion (possibly use multiple kinect).
* Must be able to develop into application for constructing virtual objects based on real world tracking.


Hardware requirment & Setup
---------------------------

Hardware & Software Requirement and Setup: iWand (kinect)

Hardware with performance is crucial part for this project.

* 8GB RAM (16 GB recommended)
* EFCI Enables Motherboard (Secure Boot Disabled)
* Core i7 CPU (0r 4+Ghz processeor for robust SIFT model analyses)
* GTX 1060 (6 GB) (recommended TWO GPU system) (Must support CUDA)

Possible problems:

* Budgeting VS performance.



Installation
------------

Ubuntu 16.04.

For Ubuntu OS installation please follow [Installation Guide (OS)](https://www.ubuntu.com/download/desktop/install-ubuntu-desktop)

CUDA
```
CUDA 8 Installation: The safest and fastest way to install CUDA Toolkit and proprietary drivers is through .deb over the network
sudo dpkg -i cuda-repo-ubuntu1604_(current_stable version)_amd64.deb
sudo apt-get update
sudo apt-get install cuda
gedit ~/.bashrc
export PATH=/usr/local/cuda-9.0/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-9.0/lib64\ ${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
```
Test your CUDA with the CUDA samples
```
cuda-install-samples-8.0.sh ~
cd ~/NVIDIA_CUDA-8.0_Samples/5_Simulations/nbody
make
./nbody
```
Possible problems:

- Matching SM architecture is crucial to CUDA based acceleration, please find the write gencode for your device (exmaple GTX 1060 is based on SM_61), to understand more you can start with the following links:

ROS Kinetic (Installation):
---------------------------

Set up your Sources list
```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
```
Set up your Keys
```
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
```
Update your repo
```
sudo apt-get update
```

Desktop Full Install
```
sudo apt-get install ros-kinetic-desktop-full
```
Initialize resdep
```
sudo rosdep init
rosdep update
```
Add Path to your system
```
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```
Additional ros tools for easy navigation and packaging
```
sudo apt-get install python-rosinstall
```



Workspace Setup
---------------

As we install ROS KINETIC, we can follow the follwing sample setup:

Load the Enviromantal setup file into your terminal:
source /opt/ros/kinetic/setup.bash

Create your WorkSpace:
```
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
```
Build your Workspace
```
cd ~/catkin_ws/
catkin_make
```

Source your new bash file:
```
source devel/setup.bash
```
```
make sure ROS_PACKAGE_PATH environment variable includes the directory you're in
```
```
echo $ROS_PACKAGE_PATH /home/youruser/catkin_ws/src:/opt/ros/kinetic/share
```

Workspace Commands
------------------

Root acess for file editing
```
gksu nautilus
```
Run the Following Commands from terminal by first nvigating to your workdirectory:

```
Run Commands (all in diffrent terminals)
```
roscore
```
roslaunch simtrack_nodes main_kinect2.launch
```
rviz
```
rostopic echo /tf > data.txt
```


Debugs:
-------
add_definitions( -fexceptions )

https://github.com/giacomodabisias/libfreenect2pclgrabber/issues/17

Build failure: https://github.com/code-iai/iai_kinect2/issues/377

Simtrack (Kinetic Version)
Solution:
Careful is advised when trying to fix theses warnings manually creating links.
I run:
sudo mv /usr/lib/nvidia-384/libEGL.so.1 /usr/lib/nvidia-384/libEGL.so.1.org
sudo mv /usr/lib32/nvidia-384/libEGL.so.1 /usr/lib32/nvidia-384/libEGL.so.1.org
sudo ln -s /usr/lib/nvidia-384/libEGL.so.384 /usr/lib/nvidia-384/libEGL.so.1
sudo ln -s /usr/lib32/nvidia-384/libEGL.so.384 /usr/lib32/nvidia-384/libEGL.so.1
And ended up unable to boot into a graphical interface. I was able to recover by running:
dpkg-reconfigure nvidia-384

Build the SIFT-model of the THREE provided demo objects. Note that an absolute path is required:
```
rosrun interface cmd_line_generate_sift_model `pwd`/src/simtrack/data/object_models/ros_fuerte/ros_fuerte.obj
rosrun interface cmd_line_generate_sift_model `pwd`/src/simtrack/data/object_models/ros_groovy/ros_groovy.obj
rosrun interface cmd_line_generate_sift_model `pwd`/src/simtrack/data/object_models/ros_hydro/ros_hydro.obj
```
